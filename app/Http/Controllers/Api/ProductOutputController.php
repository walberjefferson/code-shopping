<?php

namespace CodeShopping\Http\Controllers\Api;

use CodeShopping\Models\ProductOutput;
use CodeShopping\Http\Controllers\Controller;
use CodeShopping\Http\Requests\ProductOutputRequest;
use CodeShopping\Http\Resources\ProductOutputResource;

class ProductOutputController extends Controller
{

    public function index()
    {
        $output = ProductOutput::with('product')->paginate();
        return ProductOutputResource::collection($output);
    }

    public function store(ProductOutputRequest $request)
    {
        $input = ProductOutput::create($request->all());
        return new ProductOutputResource($input);
    }

    public function show(ProductOutput $output)
    {
        return new ProductOutputResource($output);
    }
}
