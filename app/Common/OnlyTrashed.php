<?php

namespace CodeShopping\Common;

use Illuminate\Database\Eloquent\Builder;

trait OnlyTrashed
{
    protected function onlyTrashedIfRequested(Builder $query)
    {
        if (request()->get('trashed') == 1) {
            $query->onlyTrashed();
        }
        return $query;
    }
}