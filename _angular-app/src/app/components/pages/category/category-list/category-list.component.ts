import {Component, OnInit, ViewChild} from '@angular/core';
import {HttpErrorResponse} from "@angular/common/http";
import {CategoryNewModalComponent} from "../category-new-modal/category-new-modal.component";
import {CategoryEditModalComponent} from "../category-edit-modal/category-edit-modal.component";
import {CategoryDeleteModalComponent} from "../category-delete-modal/category-delete-modal.component";
import {CategoryHttpService} from "../../../../services/http/category-http.service";
import {NotifyMessageService} from "../../../../services/notify-message.service";
import {Category} from "../../../../models";

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.css']
})
export class CategoryListComponent implements OnInit {

  categories: Array<Category> = [];

  @ViewChild(CategoryNewModalComponent)
  categoryNewModal: CategoryNewModalComponent;

  @ViewChild(CategoryEditModalComponent)
  categoryEditModal: CategoryEditModalComponent;

  @ViewChild(CategoryDeleteModalComponent)
  categoryDeleteModal: CategoryDeleteModalComponent;

  categoryId: number;

  constructor(private categoryHttp: CategoryHttpService, private notifyMessage: NotifyMessageService) {
  }

  ngOnInit() {
    this.getCategories();
  }

  getCategories() {
    this.categoryHttp.list().subscribe(response => this.categories = response.data);
  }

  showModalInsert() {
    this.categoryNewModal.showModal();
  }

  showModalEdit(categoryId: number) {
    this.categoryId = categoryId;
    this.categoryEditModal.showModal();
  }

  showModalDelete(categoryId: number) {
    this.categoryId = categoryId;
    this.categoryDeleteModal.showModal();
  }

  onInsertSucces($event: any) {
    // console.log($event);
    this.notifyMessage.success('Categoria cadastrada com sucesso.');
    this.getCategories()
  }

  onInsertError($event: HttpErrorResponse) {
    this.notifyMessage.error('Erro ao cadastrar categoria.');
    // console.log($event)
  }

  onEditSucces($event: any) {
    this.notifyMessage.success('Categoria alterada com sucesso.');
    this.getCategories()
  }

  onEditError($event: HttpErrorResponse) {
    this.notifyMessage.error('Erro ao tentar alterar categoria.');
    // console.log($event)
  }

  onDeleteSucces($event: any) {
    this.notifyMessage.success('Categoria excluida com sucesso.');
    this.getCategories()
  }

  onDeleteError($event: HttpErrorResponse) {
    this.notifyMessage.error('Erro ao tentar excluir categoria.');
    console.log($event)
  }
}
